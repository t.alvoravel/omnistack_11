//Importações
const express = require('express');

const OngController = require('./controllers/OngController');
const CasoController = require('./controllers/CasoController');
const ProfileController = require('./controllers/ProfileController');
const SessionController = require('./controllers/SessionController');

const routes = express.Router();

//Logar
routes.post('/secao', SessionController.create);

//Listar ONGs
routes.get('/ongs', OngController.index);
//Criar ONG
routes.post('/ongs', OngController.create);

//Criar Caso
routes.post('/casos', CasoController.create);
//Listar Casos
routes.get('/casos', CasoController.index);
//Apagar Caso
routes.delete('/casos/:id', CasoController.delete);

//Listar Caso de uma ONG
routes.get('/profile', ProfileController.index);

//Permite exportar o arquivo
module.exports = routes;