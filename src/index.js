const express = require('express');
//Módulo de segurança
const cors = require('cors');
//Usar as rotas definidas no arquivo routes.js
const routes = require('./routes');

const app = express();

app.use(cors());
//Define que a aplicação vai usar objetos json nas requisições a partir do Express
app.use(express.json());
app.use(routes);

//Escutar na porta 3333
app.listen(3333);